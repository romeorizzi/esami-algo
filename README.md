In questa repo condividiamo con i nostri studenti del corso di Algoritmi in UniVR l'archivo dei problemi dei vari appelli d'esame. Puoi trovare quì non solo i testi ma anche tutti i sorgenti dei probemi, inclusi i generatori di istanza, i solutori, e gli eventuali correttori.
In questa repo trovi inoltre delle guide su come:

1. prepararti all'esame,
2. procedure in atto all'esame, cose da sapere e da fare per prendervi parte,
3. politica di registrazione dei voti.

DISCLAIMER:

I materiali in questa repo sono riservati all'uso degli studenti dei miei corsi (Prof. Romeo Rizzi). Diffido altri soggetti dal farne un utilizzo fuori dalle finalità per cui originariamente intesi senza riconoscerne la fonte. Inoltre, nessun uso con finalità non esclusivamente didattiche è consentito se non dopo aver ottenuto esplicita autorizzazione.
Infine, non posso essere ritenuto responsabile di eventuali danni, di qualsiasi natura, causati da eventualie malfunzionamento di codici o altre carenze in alcun altro materiale contenuti in questa repo.
